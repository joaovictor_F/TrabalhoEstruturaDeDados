#include <iostream>
#include <stdlib.h>

using namespace std;

const int tamanho = 5;

int i;

struct registro {
        int codigo;
        char nome[60];
        char endereco[60];
};

struct registro reg[tamanho];

void incrementarRegistro (int tamanho){
    for(i=0;i<tamanho;i++){
    cout << "Insira o codigo: " << endl;
    cin >> reg[i].codigo;
    cout << "Insira o nome: " << endl;
    cin >> reg[i].nome;
    cout << "Insira o endereco: " << endl;
    cin >> reg[i].endereco;
    cout << "******************" << endl;
    }
}

void pesquisarStruct(int cod, int tam){
    for(i = 0;i < tam;i++){
        if(cod == reg[i].codigo){
            cout << "O nome e: " << reg[i].nome << "." << endl;
            cout << "O endereco e: " << reg[i].endereco << "." << endl;
            exit(0);
        }
    }
    cout << "Codigo nao encontrado, insira um codigo valido para realizar a pesquisa." << endl;
}

int main()
{
    int numero;

    incrementarRegistro(tamanho);
    cout << "Insira o numero do codigo do registro que voce deseja pesquisar: ";
    cin >> numero;
    pesquisarStruct(numero, tamanho);

    return 0;
}
