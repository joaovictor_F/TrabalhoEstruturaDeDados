#include <iostream>

using namespace std;
const int tamanho=5;

int* vetor[tamanho];

void inicializarVetor(int *vetor[],int tam){
    for (int i=0; i < tam; i++){
        vetor[i] = new int;
        *(vetor[i])=0;

    }
}

void imprimirVetor(int *vetor[],int tam){
    for (int i=0; i < tam; i++){
        cout << "Posicao "<<(i+1)<<": "<<*(vetor[i])<<endl;
    }
}

void lerVetor(int *vetor[],int tam){
    for (int i=0; i < tam; i++){
        cout << "Posicao "<<(i+1)<<": ";
        cin >> *(vetor[i]);
    }
}


void bubbleSort(int *vetor[],int tamanho){
    cout<< "Ordenacao!!!"<< endl;
    for (int i=tamanho-1; i>=0; i--){
        for (int j=0; j<i; j++){
            if (*vetor[j]>*vetor[j+1]){
                int temp = *vetor[j];
                *vetor[j]=*vetor[j+1];
                *vetor[j+1]=temp;
            }
        }
    }
}

int main()
{
    inicializarVetor(vetor,tamanho);
    lerVetor(vetor,tamanho);
    bubbleSort(vetor,tamanho);
    imprimirVetor(vetor,tamanho);
    return 0;

}
