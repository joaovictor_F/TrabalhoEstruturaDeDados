#include <iostream>

using namespace std;

const int tamanho=10;

int i,maior, menor, vetor ,valor;


struct pontos {
    int x;
    int y;
};

struct pontos p[tamanho];

void incrementarPontos (int tamanho){
    for(int i=0; i < tamanho; i++){
    cout << "Digite o valor de X na posicao " << (i+1) << ": " ;
    cin >> p[i].x;
    }
    cout << "******************" << endl;

    for(int i=0; i < tamanho; i++){
    cout << "Digite o valor de Y na posicao " << (i+1) << ": " ;
    cin >> p[i].y;
    }
}

void maiorValordeX(pontos p[], int tamanho){
    for(int i=0; i<tamanho; i++){
        if (p[i].x > maior){
            maior = p[i].x;
        }
    }
   cout << "O maior valor de X e: " << maior << endl;
}

void menorValorY(pontos p[], int tamanho){
    for(int i=0; i<tamanho; i++){
        if (p[i].y < maior){
            menor = p[i].y;
        }
    }
   cout << "O menor valor de Y e: " << menor << endl;
}

void procurarValoremXeY(pontos p[], int tamanho){
    cout << "Digite o Valor que devera ser procurado nos vetores X e Y:" ;
    cin >> valor;
    cout << endl;
    for(int i=0; i<tamanho; i++){
        if(valor == p[i].x){
           cout << "As posicoes que contem o valor no vetor X:" << (i+1) << endl;
        }
    }
    for(int i=0; i<tamanho; i++){
        if(valor == p[i].y){
            cout << "As posicoes que contem o valor no vetor Y:" << (i+1) << endl;
        }
    }
}
int main()
{
    incrementarPontos(tamanho);
    maiorValordeX(p, tamanho);
    menorValorY(p, tamanho);
    procurarValoremXeY(p, tamanho);

    return 0;
}
